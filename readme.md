# MOI Application Setup and Configuration

This is a brief guide to setting up and configuring your Django application. We are using Postgres SQL as the database,
with the following environment variables:

- `DB_NAME`: The name of the database you will connect to
- `DB_USER`: The username that your application will use to log into the database
- `DB_PASS`: The password for the `DB_USER`
- `DB_HOST`: The hostname where your database is located
- `DB_PORT`: The port on which your database is running
- `APP_ENV`: The application's environment. In this case, it's set to `dev` for development. Change as needed.

## Prerequisites

Before you start, make sure you have:

1. Python 3.7 or later installed. You can check this by running `python --version` in your terminal.
2. Postgres SQL installed and running.
3. Pip, the Python package installer. You can check by running `pip --version` in your terminal.
4. Virtualenv for creating isolated Python environments. You can check by running `virtualenv --version` in your
   terminal.
5. Optional: docker and docker-compose for running the application in a container.

## Setup Instructions

### 1. Clone the Project

`
git clone git@gitlab.com:omi4571821/app-example.git
`

Navigate into the cloned project directory:

`
cd app-example
`

### 2. Create a Virtual Environment

Create a virtual environment for your project:

`
virtualenv venv
`

Activate the virtual environment:

`
source venv/bin/activate
`

### 3. Install Dependencies

Install the project dependencies:

`
pip install -r requirements.txt
`

### 4. Create a Database

Create a database for your project. You can do this using the Postgres CLI or a GUI like Postico.

### 5. Set Environment Variables

Create a `.env` file in the root of your project and add the following environment variables:

`
DB_NAME=your_db_name
DB_USER=your_db_user
DB_PASS=your_db_pass
DB_HOST=your_db_host
DB_PORT=your_db_port
APP_ENV=dev
`

### 6. Run Migrations

Run the migrations to create the database tables:

`
python manage.py migrate
`

### 7. Run the Application

Run the application:

`
python manage.py runserver
`

### 8. Create a Superuser

Create a superuser for your application:

`
python manage.py createsuperuser
`

### 9. Access the Admin Panel

Access the admin panel at `http://localhost:8000/admin/` and log in with the superuser credentials you created in the
previous step.

### 10. Create a New App

Create a new app for your project:

`
python manage.py startapp your_app_name
`

## Docker container environment for development

##### Prerequisites

Before you start, make sure you have:
1. Docker installed and running.
2. Docker-compose installed and running.


##### Setup Instructions

1. Set the environment variables in the `.env` file.
2. Run `docker-compose up --build` to build the docker image and run the container.
3. Run `docker-compose up -d database` to run the database.
4. Run `docker-compose run --rm migrate` to  migrate table.
5. Run `docker-compose up -d web` to run web server.
6. Run `docker-compose exec web /bin/bash` to exec container.
7. Run `python manage.py createsuperuser` to create superuser.


##### Access the Admin Panel

Access the admin panel at `http://localhost:8000/admin/` and log in with the superuser credentials you created in the
previous step.

