asgiref==3.7.2
Django==4.2.2
python-decouple==3.8
python-dotenv==1.0.0
sqlparse==0.4.4
typing_extensions==4.6.3
psycopg2==2.9.1
