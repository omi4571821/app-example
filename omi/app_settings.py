# read .env file if it exists and set environment variables

from dotenv import load_dotenv
load_dotenv()

import os
ALLOWED_HOSTS = ['0.0.0.0']
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': os.environ.get('DB_NAME') if os.environ.get('DB_NAME') else 'omi',
        'USER': os.environ.get('DB_USER') if os.environ.get('DB_USER') else 'omi_user',
        'PASSWORD': os.environ.get('DB_PASS') if os.environ.get('DB_PASS') else 'omi_password',
        'HOST': os.environ.get('DB_HOST') if os.environ.get('DB_HOST') else 'localhost',
        'PORT': os.environ.get('DB_PORT') if os.environ.get('DB_PORT') else '5432',

    },
    'OPTIONS': {
        'options': '-c search_path=omi'
    }
}
